#pragma once

#include <QObject>
#include <QVector>
#include <QColor>

#include "hrana.h"


class mnohouholnik : public QObject
{
		Q_OBJECT

	public:
		mnohouholnik();
		~mnohouholnik();
		void nastavMnohouholnik(QVector<QPoint>, QColor, QColor);
		QVector<hrana> hrany;
		QColor farbaHran;
		QColor farbaVypln;
		void skalovanie(QPoint, double);
		void skosenie(QPoint, char, double);
		void otocenie(QPoint, char, double);
		void preklopenie(hrana);
		void presunutie(QPoint originalnaPozicia, QPoint novaPozicia);
};
