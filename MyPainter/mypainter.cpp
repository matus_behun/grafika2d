#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(750, 750);
	paintWidget.nastavitPaintWidget(this);

	QPixmap pixmapHrany(157, 23);
	pixmapHrany.fill(Qt::black);
	ui.label_16->setPixmap(pixmapHrany);
	QPixmap pixmapVypln(157, 23);
	pixmapVypln.fill(Qt::white);
	ui.label_17->setPixmap(pixmapVypln);

	ui.radioButton_3->setChecked(true);
}

void MyPainter::vyberFarbuHran()
{
	QColorDialog colorDialog;
	QPixmap pixmapHrany(157, 23);
	QColor color = colorDialog.getColor();
	paintWidget.nastavFarbuHran(color);
	pixmapHrany.fill(color);
	ui.label_16->setPixmap(pixmapHrany);
}

void MyPainter::vyberFarbuVyplne()
{
	QColorDialog colorDialog;
	QColor color = colorDialog.getColor();
	QPixmap pixmapVypln(157, 23);
	paintWidget.nastavFarbuVyplne(color);
	pixmapVypln.fill(color);
	ui.label_17->setPixmap(pixmapVypln);
}

void MyPainter::vykresliKruh()
{
	paintWidget.clearImage();
	paintWidget.midpointCircleAlgorithm();
}

void MyPainter::clearImage()
{
	paintWidget.clearImage();
}

void MyPainter::zobrazStatusBar(QString sprava)
{
	ui.statusBar->showMessage(sprava);
}

void MyPainter::skaluj()
{
	paintWidget.skaluj(ui.doubleSpinBox->value());
}

void MyPainter::skos()
{
	char smer;
	if (ui.radioButton->isChecked()) {
		smer = 'x';
	} else {
		smer = 'y';
	}

	paintWidget.skos(smer, ui.doubleSpinBox_2->value());
}

void MyPainter::rotuj()
{
	char smer;

	if (ui.radioButton_3->isChecked()) {
		smer = 'v';
	} else {
		smer = 'p';
	}
	paintWidget.rotuj(smer, ui.doubleSpinBox_3->value());
}

void MyPainter::preklop()
{
	paintWidget.preklop();
}

void MyPainter::rotaciaSmerHodiny()
{
	if (ui.radioButton_4->isChecked()) {
		ui.radioButton_4->setChecked(false);
	}
}

void MyPainter::rotaciaSmerProtiHodiny()
{
	if (ui.radioButton_3->isChecked()) {
		ui.radioButton_3->setChecked(false);
	}
}

void MyPainter::vykresliMnohouholnik()
{
	paintWidget.vykresliMnohouholnik();
}

void MyPainter::vykresliBezierovuKrivku()
{
	paintWidget.clearImage();
	paintWidget.casteljauAlgorithm(ui.spinBox->value());
}

void MyPainter::vykresliSpline()
{
	paintWidget.clearImage();
	paintWidget.bSpline(ui.spinBox->value());
}

void MyPainter::vykresliUsecku()
{
	paintWidget.clearImage();
	paintWidget.dda();
}

MyPainter::~MyPainter()
{
}