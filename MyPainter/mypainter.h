#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QColorDialog>
#include <QPalette>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();
public slots:
	void vyberFarbuHran();
	void vyberFarbuVyplne();
	void vykresliUsecku();
	void vykresliKruh();
	void vykresliBezierovuKrivku();
	void vykresliSpline();
	void clearImage();
	void zobrazStatusBar(QString);
	void skaluj();
	void skos();
	void rotuj();
	void preklop();
	void rotaciaSmerHodiny();
	void rotaciaSmerProtiHodiny();
	void vykresliMnohouholnik();
private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;
};

#endif // MYPAINTER_H
