#include "paintwidget.h"

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	vykreslenyMnohouholnik = false;
}

void PaintWidget::nastavitPaintWidget(MyPainter * mp)
{
	QObject::connect((QObject *) this, SIGNAL(posliSpravu(QString)), (QObject *) mp, SLOT(zobrazStatusBar(QString)));
}

void PaintWidget::nastavFarbuHran(QColor farba)
{
	farbaHran = farba;
}

void PaintWidget::nastavFarbuVyplne(QColor farba)
{
	farbaVyplne = farba;
	h.farbaVypln = farba;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());

	update();

	return true;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		kliknutia.append(event->pos());
		emit posliSpravu(QString("Pocet kliknuti: ").append(QString::number(kliknutia.size())));
		praveKliknutia.clear();
	} else if (event->button() == Qt::RightButton) {
		praveKliknutia.append(event->pos());
		emit posliSpravu(QString("Pocet pravych kliknuti: ").append(QString::number(praveKliknutia.size())));
		kliknutia.clear();
	}
	lastMovedPosition = event->pos();
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	kliknutia.clear();
	praveKliknutia.clear();
	emit posliSpravu(QString("Zoznamy kliknuti vymazane"));
}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (vykreslenyMnohouholnik) {
		clearImage();
		h.presunutie(lastMovedPosition, event->pos());
		zobrazMnohouholnik();
		lastMovedPosition = event->pos();
	}
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::vykresliVertikalnuCiaru(int x, int y, int uroven, QColor c)
{
	QPainter painter(&image);
	painter.setPen(QPen(c));

	int zaciatok;
	int koniec;

	if (x < y) {
		zaciatok = x;
		koniec = y;
	}
	else if(x > y) {
		koniec = x;
		zaciatok = y;
	} else {
		return;
	}

	for (int i = zaciatok; i <= koniec; i++) {
		painter.drawPoint(uroven, i);
	}

	update();
}

void PaintWidget::vykresliVodorovnuCiaru(int x, int y, int uroven, QColor c)
{
	QPainter painter(&image);
	painter.setPen(QPen(c));

	int zaciatok;
	int koniec;

	if (x < y) {
		zaciatok = x;
		koniec = y;
	}
	else {
		koniec = x;
		zaciatok = y;
	}

	for (int i = zaciatok; i <= koniec; i++) {
		painter.drawPoint(i, uroven);
	}

	update();
}

void PaintWidget::dda(void)
{
	QPoint a, b;

	if (kliknutia.size() < 2) {
		kliknutia.clear();
		emit posliSpravu(QString(u8"Po�et kliknut� je men�� ako 2"));
		return;
	}
	vykreslenyMnohouholnik = false;

	a = kliknutia.at(kliknutia.size() - 1);
	b = kliknutia.at(kliknutia.size() - 2);
	kliknutia.clear();

	QPoint m, n, kurzor;

	if (a.x() < b.x()) {
		m = a;
		n = b;
	}
	else if (a.x() > b.x()) {
		m = b;
		n = a;
	}
	else {
		vykresliVertikalnuCiaru(a.y(), b.y(), a.x(), farbaHran);
		return;
	}

	QPainter painter(&image);
	painter.setPen(QPen(farbaHran));

	double delta_x, delta_y, dlzka_x, dlzka_y, dkurzor_x, dkurzor_y;

	kurzor = m;
	dkurzor_x = (double)m.x();
	dkurzor_y = (double)m.y();

	dlzka_x = fabs(m.x() - n.x());
	dlzka_y = fabs(m.y() - n.y());

	if (dlzka_y <= dlzka_x) {
		delta_y = dlzka_y / dlzka_x;
		delta_x = 1;
	}
	else {
		delta_y = 1;
		delta_x = dlzka_x / dlzka_y;
	}

	if (m.y() > n.y()) {
		delta_y *= -1;
	}

	do {
		painter.drawPoint(kurzor.x(), kurzor.y());
		dkurzor_x += delta_x;
		dkurzor_y += delta_y;
		kurzor.setX((int)round(dkurzor_x));
		kurzor.setY((int)round(dkurzor_y));
	} while (kurzor.x() != n.x() || kurzor.y() != n.y());
	painter.drawPoint(kurzor.x(), kurzor.y());

	update();
}

void PaintWidget::dda(QPoint a, QPoint b)
{
	QPoint m, n, kurzor;

	if (a.x() < b.x()) {
		m = a;
		n = b;
	}
	else if (a.x() > b.x()) {
		m = b;
		n = a;
	}
	else {
		vykresliVertikalnuCiaru(a.y(), b.y(), a.x(), farbaHran);
		return;
	}

	QPainter painter(&image);
	painter.setPen(QPen(farbaHran));

	double delta_x, delta_y, dlzka_x, dlzka_y, dkurzor_x, dkurzor_y;

	kurzor = m;
	dkurzor_x = (double)m.x();
	dkurzor_y = (double)m.y();

	dlzka_x = fabs(m.x() - n.x());
	dlzka_y = fabs(m.y() - n.y());

	if (dlzka_y <= dlzka_x) {
		delta_y = dlzka_y / dlzka_x;
		delta_x = 1;
	}
	else {
		delta_y = 1;
		delta_x = dlzka_x / dlzka_y;
	}

	if (m.y() > n.y()) {
		delta_y *= -1;
	}

	do {
		painter.drawPoint(kurzor.x(), kurzor.y());
		dkurzor_x += delta_x;
		dkurzor_y += delta_y;
		kurzor.setX((int)round(dkurzor_x));
		kurzor.setY((int)round(dkurzor_y));
	} while (kurzor.x() != n.x() || kurzor.y() != n.y());
	painter.drawPoint(kurzor.x(), kurzor.y());

	update();
}

void PaintWidget::dda(QPoint a, QPoint b, QColor c)
{
	QPoint m, n, kurzor;

	if (a.x() < b.x()) {
		m = a;
		n = b;
	} else if (a.x() > b.x()) {
		m = b;
		n = a;
	} else {
		vykresliVertikalnuCiaru(a.y(), b.y(), a.x(), farbaHran);
		return;
	}

	QPainter painter(&image);
	painter.setPen(QPen(c));

	double delta_x, delta_y, dlzka_x, dlzka_y, dkurzor_x, dkurzor_y;

	kurzor = m;
	dkurzor_x = (double)m.x();
	dkurzor_y = (double)m.y();

	dlzka_x = fabs(m.x() - n.x());
	dlzka_y = fabs(m.y() - n.y());

	if (dlzka_y <= dlzka_x) {
		delta_y = dlzka_y / dlzka_x;
		delta_x = 1;
	}
	else {
		delta_y = 1;
		delta_x = dlzka_x / dlzka_y;
	}

	if (m.y() > n.y()) {
		delta_y *= -1;
	}

	do {
		painter.drawPoint(kurzor.x(), kurzor.y());
		dkurzor_x += delta_x;
		dkurzor_y += delta_y;
		kurzor.setX((int)round(dkurzor_x));
		kurzor.setY((int)round(dkurzor_y));
	} while (kurzor.x() != n.x() || kurzor.y() != n.y());
	painter.drawPoint(kurzor.x(), kurzor.y());

	update();
}

void PaintWidget::midpointCircleAlgorithm(void)
{
	QPoint a, b;
	QPainter painter(&image);
	QVector<int> ys;
	painter.setPen(QPen(farbaHran));

	if (kliknutia.size() < 2) {
		kliknutia.clear();
		emit posliSpravu(QString(u8"Po�et kliknut� je men�� ako 2"));
		return;
	}
	vykreslenyMnohouholnik = false;
	a = kliknutia.at(kliknutia.size() - 1);
	b = kliknutia.at(kliknutia.size() - 2);
	kliknutia.clear();

	int r = round(sqrt(((b.x() - a.x()) * (b.x() - a.x())) + (b.y() - a.y() * (b.y() - a.y()))));

	int x = 0;
	int y = r;
	int p = 1 - r;

	mcaDrawLine(a.x() + y, a.x() - y, a.y() + x, &painter, true);
	mcaDrawLine(a.x() + x, a.x() - x, a.y() + y, &painter, true);
	mcaDrawLine(a.x() + x, a.x() - x, a.y() - y, &painter, true);
	mcaDrawLine(a.x() - y, a.x() + y, a.y() - x, &painter, true);
	ys << a.y() + y << a.y() - y << a.y() + x << a.y() - x;


	for (x = 0; x < y; x++) {
		if (p < 0) {
			p += 2 * x + 3;
		} else {
			p += 2 * (x - y) + 5;
			y--;
		}

		if (ys.contains(a.y() + x)) {
			mcaDrawLine(a.x() + y, a.x() - y, a.y() + x, &painter, false);
		} else {
			mcaDrawLine(a.x() + y, a.x() - y, a.y() + x, &painter, true);
		}
		ys << a.y() + x;

		if (ys.contains(a.y() + y)) {
			mcaDrawLine(a.x() + x, a.x() - x, a.y() + y, &painter, false);
		} else {
			mcaDrawLine(a.x() + x, a.x() - x, a.y() + y, &painter, true);
		}
		ys << a.y() + y;

		if (ys.contains(a.y() - y)) {
			mcaDrawLine(a.x() + x, a.x() - x, a.y() - y, &painter, false);
		} else {
			mcaDrawLine(a.x() + x, a.x() - x, a.y() - y, &painter, true);
		}
		ys << a.y() - y;

		if (ys.contains(a.y() - x)) {
			mcaDrawLine(a.x() - y, a.x() + y, a.y() - x, &painter, false);
		} else {
			mcaDrawLine(a.x() - y, a.x() + y, a.y() - x, &painter, true);
		}
		ys << a.y() - x;

	}

	update();
	kliknutia.clear();
}

void PaintWidget::mcaDrawLine(int x1, int x2, int uroven, QPainter *q, bool kresliOkraje)
{
	q->setPen(QPen(farbaHran));
	q->drawPoint(x1, uroven);
	q->drawPoint(x2, uroven);
	int a, b;

	if (x1 < x2) {
		a = x1;
		b = x2;
	} else {
		a = x2;
		b = x1;
	}

	if (kresliOkraje) {
		q->setPen(QPen(farbaVyplne));
		for (int i = a + 1; i < b; i++) {
			q->drawPoint(i, uroven);
		}
	}
}

void PaintWidget::casteljauAlgorithm(int pocetKrokov)
{
	if (kliknutia.size() < 2) {
		kliknutia.clear();
		emit posliSpravu(QString(u8"Po�et kliknut� je men�� ako 2"));
		return;
	}
	vykreslenyMnohouholnik = false;

	QVector<QVector<QPointF>> p;
	QPointF b = kliknutia.at(0);
	int pocetBodov = kliknutia.size();
	double t = 0;
	double deltaT = 1.0 / pocetKrokov;

	for (int i = 0; i < pocetBodov; i++) {
		QVector<QPointF> v(pocetBodov);
		v[0].setX(kliknutia.at(i).x());
		v[0].setY(kliknutia.at(i).y());
		p << v;
	}

	while (t < 1.0) {
		t += deltaT;
		for (int j = 1; j < pocetBodov; j++) {
			for (int i = 0; i < pocetBodov - j; i++) {
				p[i][j].setX((1-t)*p[i][j-1].x() + t*p[i+1][j-1].x());
				p[i][j].setY((1-t)*p[i][j-1].y() + t*p[i+1][j-1].y());
			}
		}
		dda(b.toPoint(), p[0][pocetBodov-1].toPoint());
		b = p[0][pocetBodov-1].toPoint();
	}

	kliknutia.clear();
}

QPoint funkciaPt(double t, QVector<QPoint> p)
{
	int	p0x = p.at(0).x();
	int	p1x = p.at(1).x();
	int	p2x = p.at(2).x();
	int	p3x = p.at(3).x();
	int	p0y = p.at(0).y();
	int	p1y = p.at(1).y();
	int	p2y = p.at(2).y();
	int	p3y = p.at(3).y();

	double t1 = t;
	double t2 = t * t;
	double t3 = t * t * t;

	double a0 = double(
					   double(-1.0 * (1.0 / 6.0) * t3) + 
		               double((1.0 /2.0) * t2) + 
		               double(-1.0 * (1.0 / 2.0) * t1) + 
		               double((1.0 / 6.0))
		              );
	double a1 = double(
					   double((1.0 / 2.0) * t3) +
					   double(-1.0 * t2) +
					   double((2.0/3.0))
					  );
	double a2 = double(
					   double(-1.0 * (1.0 / 2.0) * t3) +
					   double((1.0 / 2.0) * t2) +
					   double((1.0 / 2.0) * t1) +
					   double((1.0 / 6.0))
					  );
	double a3 = double(
					   double((1.0 / 6.0) * t3)
		              );

	int x = int(round(a0 * p0x + a1 * p1x + a2 * p2x + a3 * p3x));
	int y = int(round(a0 * p0y + a1 * p1y + a2 * p2y + a3 * p3y));

	return QPoint(x, y);
}

void PaintWidget::bSpline(int pocetKrokov)
{
	if (kliknutia.size() < 4) {
		kliknutia.clear();
		emit posliSpravu(QString(u8"Po�et kliknut� je men�� ako 4"));
		return;
	}
	vykreslenyMnohouholnik = false;

	QPainter painter(&image);
	painter.setPen(QPen(farbaHran));

	QVector<QPoint> body;
	QPoint a;

	for (int i = 0; i < kliknutia.size() - 3; i++) {
		for (int j = 0; j < pocetKrokov; j++) {
			a = funkciaPt(double(double(j) / double(pocetKrokov)), kliknutia.mid(i, 4));
			body.append(a);
		}
	}

	for (int i = 0; i < body.size(); i++) {
		painter.drawPoint(body.at(i));
	}

	kliknutia.clear();
	update();
}

void PaintWidget::skos(char smer, double velkost)
{
	if (vykreslenyMnohouholnik) {
		clearImage();
		h.skosenie(kliknutia.last(), smer, velkost);
		zobrazMnohouholnik();
	}
}

void PaintWidget::skaluj(double velkost)
{
	if (vykreslenyMnohouholnik) {
		clearImage();
		h.skalovanie(kliknutia.last(), velkost);
		zobrazMnohouholnik();
	}
}

void PaintWidget::rotuj(char smer, double uhol)
{
	if (vykreslenyMnohouholnik) {
		clearImage();
		h.otocenie(kliknutia.last(), smer, uhol);
		zobrazMnohouholnik();
	}
}

void PaintWidget::preklop()
{
	if (praveKliknutia.size() < 2 || !vykreslenyMnohouholnik) {
		return;
	}

	printf("Skusam preklopit\n");
	hrana m(praveKliknutia.last(), praveKliknutia.at(praveKliknutia.size() - 2));
	printf("[%d, %d] ~> [%d, %d]\n", m.dajA().x(), m.dajA().y(), m.dajB().x(), m.dajB().y());
	h.preklopenie(m);
	zobrazMnohouholnik();
}

bool cmp_dolneY_dolneX_smernica(const hrana &a, const hrana &b)
{
	return (a.dajDolneY() > b.dajDolneY()) ||
		((a.dajDolneY() == b.dajDolneY()) && (a.dajDolneX() < b.dajDolneX())) ||
		((a.dajDolneY() == b.dajDolneY()) && (a.dajDolneX() == b.dajDolneX()) && (a.dajSmernicu() < b.dajSmernicu()));
}

void PaintWidget::vyplnUzavretuOblast(QList<hrana> hrany, QColor c)

{
	QList<hrana> aktivneHrany;
	printf("hrany: %d\n", hrany.size());

	qSort(hrany.begin(), hrany.end(), cmp_dolneY_dolneX_smernica);

	/* Nastav na hranu s najvacsim Y */
	int urovenY = hrany.first().dajDolneY() - 1;
	QMutableListIterator<hrana> aktivneHranyIter(aktivneHrany);
	QVector<int> priesecniky;
	int hranyIter = 0;
	do {
		/* Pridavanie hran k aktivnym */
		for (; hranyIter < hrany.size() && (hrany[hranyIter].dajDolneY() - 1) == urovenY; hranyIter++) {
			/* Pridavam prvu hranu do zoznamu aktivnych hran */
			if (aktivneHrany.empty()) {
				aktivneHrany << hrany[hranyIter];
				/* Pridavam do neprazdneho zoznamu */
			}
			else {
				int miestoVlozenia;
				/* Najdi miesto vlozenia a vloz */
				for (miestoVlozenia = 0; miestoVlozenia < aktivneHrany.size() && hrany[hranyIter].dajPriesecnik() > round(aktivneHrany[miestoVlozenia].dajPriesecnik()); miestoVlozenia++)
					;
				aktivneHrany.insert(miestoVlozenia, hrany.at(hranyIter));
			}
		}

		/* Vymazavanie z aktivnych hran */
		aktivneHranyIter.toFront();
		while (aktivneHranyIter.hasNext()) {
			if (aktivneHranyIter.next().dajHorneY() > urovenY) {
				aktivneHranyIter.remove();
			}
		}


		/* Vytvaranie a aktualizacia priesecnikov */
		aktivneHranyIter.toFront();
		while (aktivneHranyIter.hasNext()) {
			priesecniky << int(aktivneHranyIter.next().aktualizujPriesecnik());
		}

		qSort(priesecniky);

		/* Vykreslovanie horizontalnych ciar */
		for (int i = 0; i < priesecniky.size() - 1; i = i + 2) {
			vykresliVodorovnuCiaru(priesecniky[i], priesecniky[i + 1], urovenY, c);
		}
		priesecniky.clear();

		urovenY--;
	} while (!aktivneHrany.empty());
}

void PaintWidget::zobrazMnohouholnik()
{
	QList<hrana> hranyVypln;

	for(int i = 0; i < h.hrany.size(); i++) {
		if (!hrana::jeVodorovna(h.hrany[i].dajA(), h.hrany[i].dajB())) {
			hranyVypln.append(h.hrany.at(i));
		}
	}

	vyplnUzavretuOblast(hranyVypln, h.farbaVypln);

	for (int i = 0; i < h.hrany.size(); i++) {
		dda(h.hrany[i].dajA(), h.hrany[i].dajB(), h.farbaHran);
	}
}

void PaintWidget::vykresliMnohouholnik(void)
{
	h.hrany.clear();
	h.nastavMnohouholnik(kliknutia, farbaHran, farbaVyplne);
	zobrazMnohouholnik();
	kliknutia.clear();
	vykreslenyMnohouholnik = true;
}
