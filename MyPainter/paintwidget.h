#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QVector>
#include <QPoint>
#include <QPointF>
#include <QPalette>
#include <stdlib.h>

#include "hrana.h"
#include "mnohouholnik.h"

class MyPainter;

class PaintWidget : public QWidget
{
		Q_OBJECT

	public:
		PaintWidget(QWidget *parent = 0);
		bool vykreslenyMnohouholnik;
		void nastavitPaintWidget(MyPainter *mp);
		void nastavFarbuHran(QColor); 
		void nastavFarbuVyplne(QColor); 
		bool newImage(int x, int y);
		void clearImage();
		void dda(void);
		void dda(QPoint, QPoint);
		void dda(QPoint, QPoint, QColor);
		void midpointCircleAlgorithm();
		void casteljauAlgorithm(int);
		void bSpline(int);
		void skos(char, double);
		void skaluj(double);
		void rotuj(char, double);
		void preklop(void);
		void vyplnUzavretuOblast(QList<hrana> hrany, QColor c);
		void vykresliMnohouholnik(void);

	protected:
		void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
		void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
		void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
		void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
		void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
		void mouseDoubleClickEvent(QMouseEvent *event);

	signals:
		void posliSpravu(QString);

	private:
		QImage image;
		QPoint lastMovedPosition;
		QColor farbaHran = Qt::black;
		QColor farbaVyplne = Qt::white;
		QVector<QPoint> kliknutia;
		QVector<QPoint> praveKliknutia;
		mnohouholnik h;
		

		void resizeImage(QImage *image, const QSize &newSize);
		void vykresliVertikalnuCiaru(int x, int y, int uroven, QColor c);
		void vykresliVodorovnuCiaru(int x, int y, int uroven, QColor c);
		void mcaDrawLine(int x, int y, int uroven, QPainter * q, bool kresliOkraje);
		void zobrazMnohouholnik();
};

#endif // PAINTWIDGET_H
