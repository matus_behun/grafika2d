/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_7;
    QLabel *label_20;
    QLabel *label;
    QFormLayout *formLayout_3;
    QPushButton *pushButton_2;
    QPushButton *pushButton_4;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_21;
    QLabel *label_4;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QPushButton *pushButton_8;
    QLabel *label_18;
    QLabel *label_3;
    QPushButton *pushButton_6;
    QPushButton *pushButton_5;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpinBox *spinBox;
    QLabel *label_19;
    QLabel *label_14;
    QLabel *label_5;
    QPushButton *pushButton_9;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_9;
    QLabel *label_7;
    QRadioButton *radioButton;
    QLabel *label_8;
    QRadioButton *radioButton_2;
    QDoubleSpinBox *doubleSpinBox_2;
    QPushButton *pushButton_10;
    QLabel *label_10;
    QFormLayout *formLayout;
    QRadioButton *radioButton_3;
    QLabel *label_11;
    QRadioButton *radioButton_4;
    QLabel *label_12;
    QDoubleSpinBox *doubleSpinBox_3;
    QLabel *label_13;
    QPushButton *pushButton_11;
    QLabel *label_15;
    QPushButton *pushButton_12;
    QSpacerItem *verticalSpacer;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(1042, 814);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(10);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(10, 10, 10, 10);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));

        verticalLayout_2->addWidget(pushButton_7);

        label_20 = new QLabel(centralWidget);
        label_20->setObjectName(QStringLiteral("label_20"));

        verticalLayout_2->addWidget(label_20);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label);

        formLayout_3 = new QFormLayout();
        formLayout_3->setSpacing(6);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        formLayout_3->setContentsMargins(0, 0, 0, 0);
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, pushButton_2);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, pushButton_4);

        label_16 = new QLabel(centralWidget);
        label_16->setObjectName(QStringLiteral("label_16"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, label_16);

        label_17 = new QLabel(centralWidget);
        label_17->setObjectName(QStringLiteral("label_17"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, label_17);


        verticalLayout_2->addLayout(formLayout_3);

        label_21 = new QLabel(centralWidget);
        label_21->setObjectName(QStringLiteral("label_21"));

        verticalLayout_2->addWidget(label_21);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setTextFormat(Qt::AutoText);
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_4);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout_2->addWidget(pushButton);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        verticalLayout_2->addWidget(pushButton_3);

        pushButton_8 = new QPushButton(centralWidget);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));

        verticalLayout_2->addWidget(pushButton_8);

        label_18 = new QLabel(centralWidget);
        label_18->setObjectName(QStringLiteral("label_18"));

        verticalLayout_2->addWidget(label_18);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_2->addWidget(label_3);

        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        verticalLayout_2->addWidget(pushButton_6);

        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        verticalLayout_2->addWidget(pushButton_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximum(1000000);
        spinBox->setValue(1000);

        horizontalLayout_2->addWidget(spinBox);


        verticalLayout_2->addLayout(horizontalLayout_2);

        label_19 = new QLabel(centralWidget);
        label_19->setObjectName(QStringLiteral("label_19"));

        verticalLayout_2->addWidget(label_19);

        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_14);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout_2->addWidget(label_5);

        pushButton_9 = new QPushButton(centralWidget);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));

        verticalLayout_2->addWidget(pushButton_9);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setSingleStep(0.1);
        doubleSpinBox->setValue(1);

        verticalLayout_2->addWidget(doubleSpinBox);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout_2->addWidget(label_6);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_3->addWidget(label_9);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout_3->addWidget(label_7);

        radioButton = new QRadioButton(centralWidget);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setChecked(true);

        horizontalLayout_3->addWidget(radioButton);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_3->addWidget(label_8);

        radioButton_2 = new QRadioButton(centralWidget);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));

        horizontalLayout_3->addWidget(radioButton_2);


        verticalLayout_2->addLayout(horizontalLayout_3);

        doubleSpinBox_2 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setSingleStep(0.1);
        doubleSpinBox_2->setValue(1);

        verticalLayout_2->addWidget(doubleSpinBox_2);

        pushButton_10 = new QPushButton(centralWidget);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));

        verticalLayout_2->addWidget(pushButton_10);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        verticalLayout_2->addWidget(label_10);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        radioButton_3 = new QRadioButton(centralWidget);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setChecked(false);
        radioButton_3->setAutoExclusive(false);

        formLayout->setWidget(0, QFormLayout::FieldRole, radioButton_3);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_11);

        radioButton_4 = new QRadioButton(centralWidget);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setAutoExclusive(false);

        formLayout->setWidget(1, QFormLayout::FieldRole, radioButton_4);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_12);

        doubleSpinBox_3 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setDecimals(5);
        doubleSpinBox_3->setValue(3.14159);

        formLayout->setWidget(2, QFormLayout::FieldRole, doubleSpinBox_3);

        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_13);


        verticalLayout_2->addLayout(formLayout);

        pushButton_11 = new QPushButton(centralWidget);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));

        verticalLayout_2->addWidget(pushButton_11);

        label_15 = new QLabel(centralWidget);
        label_15->setObjectName(QStringLiteral("label_15"));

        verticalLayout_2->addWidget(label_15);

        pushButton_12 = new QPushButton(centralWidget);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));

        verticalLayout_2->addWidget(pushButton_12);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout_2);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 760, 754));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout->addWidget(scrollArea);


        verticalLayout->addLayout(horizontalLayout);

        MyPainterClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        retranslateUi(MyPainterClass);
        QObject::connect(pushButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(vyberFarbuHran()));
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(vykresliUsecku()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(vykresliKruh()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), MyPainterClass, SLOT(vyberFarbuVyplne()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), MyPainterClass, SLOT(vykresliBezierovuKrivku()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), MyPainterClass, SLOT(vykresliSpline()));
        QObject::connect(pushButton_7, SIGNAL(clicked()), MyPainterClass, SLOT(clearImage()));
        QObject::connect(pushButton_9, SIGNAL(clicked()), MyPainterClass, SLOT(skaluj()));
        QObject::connect(pushButton_10, SIGNAL(clicked()), MyPainterClass, SLOT(skos()));
        QObject::connect(pushButton_11, SIGNAL(clicked()), MyPainterClass, SLOT(rotuj()));
        QObject::connect(pushButton_12, SIGNAL(clicked()), MyPainterClass, SLOT(preklop()));
        QObject::connect(radioButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(rotaciaSmerHodiny()));
        QObject::connect(radioButton_4, SIGNAL(clicked()), MyPainterClass, SLOT(rotaciaSmerProtiHodiny()));
        QObject::connect(pushButton_8, SIGNAL(clicked()), MyPainterClass, SLOT(vykresliMnohouholnik()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", Q_NULLPTR));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", Q_NULLPTR));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", Q_NULLPTR));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", Q_NULLPTR));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", Q_NULLPTR));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", Q_NULLPTR));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", Q_NULLPTR));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", Q_NULLPTR));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", Q_NULLPTR));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", Q_NULLPTR));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", Q_NULLPTR));
        pushButton_7->setText(QApplication::translate("MyPainterClass", "Vyma\305\276 pl\303\241tno", Q_NULLPTR));
        label_20->setText(QApplication::translate("MyPainterClass", "--------------------------------------------------------", Q_NULLPTR));
        label->setText(QApplication::translate("MyPainterClass", "V\303\275ber farieb", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MyPainterClass", "Farba hr\303\241n", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MyPainterClass", "Farba v\303\275plne", Q_NULLPTR));
        label_16->setText(QString());
        label_17->setText(QString());
        label_21->setText(QApplication::translate("MyPainterClass", "--------------------------------------------------------", Q_NULLPTR));
        label_4->setText(QApplication::translate("MyPainterClass", "Kreslenie grafick\303\275ch \303\272tvarov", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MyPainterClass", "\303\232se\304\215ka", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("MyPainterClass", "Kru\305\276nica", Q_NULLPTR));
        pushButton_8->setText(QApplication::translate("MyPainterClass", "Kresli Mnohouholn\303\255k", Q_NULLPTR));
        label_18->setText(QApplication::translate("MyPainterClass", "--------------------------------------------------------", Q_NULLPTR));
        label_3->setText(QApplication::translate("MyPainterClass", "Krivky", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("MyPainterClass", "Uniformn\303\241 neracion\303\241lna kubick\303\241 b-spline krivka", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("MyPainterClass", "Bezi\303\251rova krivka", Q_NULLPTR));
        label_2->setText(QApplication::translate("MyPainterClass", "Po\304\215et krokov", Q_NULLPTR));
        label_19->setText(QApplication::translate("MyPainterClass", "--------------------------------------------------------", Q_NULLPTR));
        label_14->setText(QApplication::translate("MyPainterClass", "Transform\303\241cie", Q_NULLPTR));
        label_5->setText(QApplication::translate("MyPainterClass", "\305\240k\303\241lovanie", Q_NULLPTR));
        pushButton_9->setText(QApplication::translate("MyPainterClass", "\305\240k\303\241luj", Q_NULLPTR));
        label_6->setText(QApplication::translate("MyPainterClass", "Skosenie", Q_NULLPTR));
        label_9->setText(QApplication::translate("MyPainterClass", " Smer: ", Q_NULLPTR));
        label_7->setText(QApplication::translate("MyPainterClass", "x", Q_NULLPTR));
        radioButton->setText(QString());
        label_8->setText(QApplication::translate("MyPainterClass", "y", Q_NULLPTR));
        radioButton_2->setText(QString());
        pushButton_10->setText(QApplication::translate("MyPainterClass", "Skos", Q_NULLPTR));
        label_10->setText(QApplication::translate("MyPainterClass", "Rot\303\241cia", Q_NULLPTR));
        radioButton_3->setText(QString());
        label_11->setText(QApplication::translate("MyPainterClass", "V smere hodinov\303\275ch ru\304\215i\304\215iek", Q_NULLPTR));
        radioButton_4->setText(QString());
        label_12->setText(QApplication::translate("MyPainterClass", "Proti smeru hodinov\303\275ch ru\304\215i\304\215iek", Q_NULLPTR));
        label_13->setText(QApplication::translate("MyPainterClass", "Uhol(radi\303\241ny)", Q_NULLPTR));
        pushButton_11->setText(QApplication::translate("MyPainterClass", "Rotuj", Q_NULLPTR));
        label_15->setText(QApplication::translate("MyPainterClass", "Preklopenie", Q_NULLPTR));
        pushButton_12->setText(QApplication::translate("MyPainterClass", "Preklop", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
