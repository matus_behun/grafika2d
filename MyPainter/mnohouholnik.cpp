#include "mnohouholnik.h"

mnohouholnik::mnohouholnik()
{
}

mnohouholnik::~mnohouholnik()
{
}

void mnohouholnik::nastavMnohouholnik(QVector<QPoint> body, QColor farbaHran, QColor farbaVypln)
{
	for (int i = 0; i < body.size() - 1; i++) {
		hrana h(body.at(i), body.at(i + 1));
		hrany.append(h);
	}

	hrana h(body.last(), body.first());
	hrany.append(h);

	this->farbaHran = farbaHran;
	this->farbaVypln = farbaVypln;
}

void mnohouholnik::skalovanie(QPoint a, double velkost)
{
	for (int i = 0; i < hrany.size(); i++) {
		QPoint m = hrany[i].dajA();
		QPoint n = hrany[i].dajB();

		QPoint relativnyVektor = m - a;
		relativnyVektor *= velkost;
		m = a + relativnyVektor;

		relativnyVektor = n - a;
		relativnyVektor *= velkost;
		n = a + relativnyVektor;

		hrany[i].nastavHranu(m, n);
	}
}

void mnohouholnik::skosenie(QPoint a, char os, double velkost)
{
	for (int i = 0; i < hrany.size(); i++) {
		QPoint m = hrany[i].dajA();
		QPoint n = hrany[i].dajB();
		if (os == 'x') {
			m.setX(m.x() + velkost * (a.y() - m.y()));
			n.setX(n.x() + velkost * (a.y() - n.y()));
		}
		else if (os == 'y') {
			m.setY(m.y() + velkost * (m.x() - a.x()));
			n.setY(n.y() + velkost * (n.x() - a.x()));
		}
		hrany[i].nastavHranu(m, n);
	}
}

void mnohouholnik::otocenie(QPoint os, char smer, double velkost)
{
	for (int i = 0; i < hrany.size(); i++) {
		QPoint m, n;
		double deltaXA = double(double(hrany[i].dajA().x()) - double(os.x()));
		double deltaYA = double(double(hrany[i].dajA().y()) - double(os.y()));
		double deltaXB = double(double(hrany[i].dajB().x()) - double(os.x()));
		double deltaYB = double(double(hrany[i].dajB().y()) - double(os.y()));

		if (smer == 'p') {
			m.setX(round(deltaXA * cos(velkost) - deltaYA * sin(velkost) + os.x()));
			m.setY(round(deltaXA * sin(velkost) + deltaYA * cos(velkost) + os.y()));

			n.setX(round(deltaXB * cos(velkost) - deltaYB * sin(velkost) + os.x()));
			n.setY(round(deltaXB * sin(velkost) + deltaYB * cos(velkost) + os.y()));
		}
		else if (smer == 'v') {
			m.setX(round(deltaXA * cos(velkost) + deltaYA * sin(velkost) + os.x()));
			m.setY(round(-deltaXA * sin(velkost) + deltaYA * cos(velkost) + os.y()));

			n.setX(round(deltaXB * cos(velkost) + deltaYB * sin(velkost) + os.x()));
			n.setY(round(-deltaXB * sin(velkost) + deltaYB * cos(velkost) + os.y()));
		}

		hrany[i].nastavHranu(m, n);
	}
}

void mnohouholnik::preklopenie(hrana os)
{
	return;

	for (int i = 0; i < hrany.size(); i++) {
		QPoint m, n;
		int a, b, c;

		hrany[i].nastavHranu(m, n);
	}
}

void mnohouholnik::presunutie(QPoint originalnaPozicia, QPoint novaPozicia)
{
	int deltaX = novaPozicia.x() - originalnaPozicia.x();
	int deltaY = novaPozicia.y() - originalnaPozicia.y();

	for (int i = 0; i < hrany.size(); i++) {
		hrany[i].presunX(deltaX);
		hrany[i].presunY(deltaY);
	}
}